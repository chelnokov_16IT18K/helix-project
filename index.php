<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Задание по практике ПМ.01</title>
    <link rel="stylesheet" href="style.css" type="text/css">

</head>

<body bgcolor =  white>
    <header>
        <h1>
            <marquee scrolldelay="40" truespeed>
                Рисование спирали в нечётной квадратной матрице с заданными размерами
            </marquee>
        </h1>
    </header>

    <main>
        <br>
        <br>
        <h1>Введите размеры матрицы:</h1>
        <form method='GET'>
            Введите ширину квадратной матрицы: <input type='text' name = 'm'><br>
            (Введённая величина должна быть числом от пяти и больше)
            <br><br>
           <input name = "submit" type ='submit'>
        </form>
        <br>
        <div>
            <?php
                $width = $_GET['m']; //ширина матрицы
                if ($width < 5){
                    throw new Exception('Ошибка ввода данных. Попробуйте ещё.');
                }
                try{
                    /**Инициализирует матрицу с указанной шириной  */
                    $matrix = array();
                    for($i = 0; $i < $width; $i++){
                        for($j = 0; $j < $width; $j++){
                            $matrix[$i][$j] = 0;
                        }
                    }

                    /**координаты центра в матрице */
                    $center = ($width -1) / 2;

                    $i = $center;
                    $j = $center;

                    if(($width % 4) == 0){//смещается точка начала рисования
                        $i = $center + 1;
                    }

                    /**количество ячеек для сдвигов по горизонтали и вертикали */
                    $a = 2;
                    $b = $a;

                    $count = 1;//счётчик с новым значением ячейки
                    while(($matrix[$width - 1][$width - 1] == 0) & ($matrix[$width - 2][$width - 1] == 0)){
                        while($b !== 0){//сдвиг вправо
                            $matrix[$i][$j] = $count;
                            $count++;

                            $j++;
                            $b--;
                        }
                        $b = $a;
                        while($b !== 0){//сдвиг вверх
                            $matrix[$i][$j] = $count;
                            $count++;
                            $i--;
                            $b--;
                        }
                        $a = $a + 2;
                        $b = $a;
                        while($b !== 0){//сдвиг влево
                            $matrix[$i][$j] = $count;
                            $count++;
                            $j--;
                            $b--;
                        }
                        $b = $a;


                        while($b !== 0){//сдвиг вниз
                            if(($matrix[0][0] !== 0) & ($width % 4 == 0)){
                                break;
                            }
                            $matrix[$i][$j] = $count;
                            $count++;
                            $i++;
                            $b--;
                        }
                        $a = $a + 2;
                        $b = $a;

                    }
                    /**Вывод матрицы с отрисованной спиралью */
                    echo "Матрица $width x $width со спиралью:";
                    echo "<table border = \"1\" cellpadding = \"5px\">";
                    for($i = 0; $i < $width; $i++){
                        echo"<tr>";
                        for($j = 0; $j < $width; $j++){
                            if($matrix[$i][$j] !== 0){
                                echo "<td bgcolor = \"white\"><font color = \"red\">".$matrix[$i][$j]."</font></td>";
                            }else{
                            echo "<td>".$matrix[$i][$j]."</td>";
                            }
                        }
                        echo "</tr>";
                    }
                    echo "</table><br>  ";

                    /**Вывод транстонированной матрицы со спиралью */
                    echo"Транспонированная матрица со спиралью:<br>";
                    echo "<table border = \"1\" cellpadding = \"5px\">";
                    for($i = 0; $i < $width; $i++){
                        echo"<tr>";
                        for($j = 0; $j < $width; $j++){
                            if($matrix[$j][$i] !==0){
                                echo "<td bgcolor = \"white\"><font color = \"red\">".$matrix[$j][$i]."</font></td>";
                            }else{
                            echo "<td>".$matrix[$j][$i]."</td>";
                            }
                        }
                        echo "</tr>";
                    }
                    echo "</table><br><br>";
                }catch(Exception $e) {
                    echo $e->getMessage()."\n";
                }
            ?>
        </div>
    </main>

    <footer>
        <center>CHELNOKOV E>I. 16IT18K, <br>
                 Penza, 2019
        </center>
    </footer>
</body>
</html>